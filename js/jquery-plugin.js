(function ($) {
    /**
     *
     * TODO: Vertical Cover
     * TODO: Initial position
     * TODO: Try with content
     * TODO: CSS animations
     * TODO: Check browser compatibility
     *
     * jQuery plugin: before/after
     *
     * @param options
     * @returns {jQuery}
     */
    $.fn.beforeAfter = function (options) {

        var attachedElement;
        var frontElement;
        var backElement;

        var settings = $.extend({
            frontElement: '',
            backElement: '',
            frontElementImage: '',
            backElementImage: '',
            draggerClass: 'ba-tools',
            preventImageDrag: true,
            allowClick: true,
            animationSpeed: 600,
            animationEasing: 'swing',
            direction: 'horizontal',
            freeHeight: false,
            // events
            onDragStart: function () {

            },
            onDragStop: function () {

            },
            isDragging: function () {

            }
        }, options);

        this.each(function () {

            attachedElement = $(this);

            frontElement = attachedElement.find(settings.frontElement);
            backElement = attachedElement.find(settings.backElement);

            frontElement.find(settings.frontElementImage).css({
                "width": attachedElement.width()
            });

            backElement.find(settings.backElementImage).css({
                "width": attachedElement.width()
            });

            var backElementStyles = {
                css: {
                    "position": "absolute",
                    "top": 0,
                    "bottom": 0,
                    "left": 0,
                    "right": 0,
                    "-webkit-touch-callout": "none",
                    "-webkit-user-select": "none",
                    "-khtml-user-select": "none",
                    "-moz-user-select": "none",
                    "-ms-user-select": "none",
                    "-o-user-select": "none",
                    "user-select": "none"
                }
            };


            var frontElementStyles = {
                css: {
                    "position": "absolute",
                    "zIndex": 1,
                    "width": "50%",
                    "overflow": "hidden",
                    "-webkit-touch-callout": "none",
                    "-webkit-user-select": "none",
                    "-khtml-user-select": "none",
                    "-moz-user-select": "none",
                    "-ms-user-select": "none",
                    "-o-user-select": "none",
                    "user-select": "none"
                },
                cssVertical: {
                    "position": "absolute",
                    "zIndex": 1,
                    "height": "50%",
                    "overflow": "hidden",
                    "-webkit-touch-callout": "none",
                    "-webkit-user-select": "none",
                    "-khtml-user-select": "none",
                    "-moz-user-select": "none",
                    "-ms-user-select": "none",
                    "-o-user-select": "none",
                    "user-select": "none"
                }
            };


            if(!settings.freeHeight){
                attachedElement.css({
                    "position": "relative",
                    "height": frontElement.height()
                });
            }

            if(settings.direction == 'vertical'){
                frontElement.css(frontElementStyles.cssVertical);
            }else{
                frontElement.css(frontElementStyles.css);
            }

            backElement.css(backElementStyles.css);

            var toolsStyles = {
                css: {
                    "position": "absolute",
                    "top": 0,
                    "bottom": 0,
                    "left": "50%",
                    "right": 0,
                    "width": "4px"
                },
                cssVertical: {
                    "position": "absolute",
                    "top": "50%",
                    "bottom": 0,
                    "left": 0,
                    "right": 0,
                    "height": "4px"
                }
            };

            var toolsElement;

            if(settings.direction == 'vertical'){
                 toolsElement = $("<div />")
                    .addClass(settings.draggerClass)
                    .css(toolsStyles.cssVertical)
                    .appendTo(attachedElement);
            }else{
                 toolsElement = $("<div />")
                    .addClass(settings.draggerClass)
                    .css(toolsStyles.css)
                    .appendTo(attachedElement);
            }

            var startDragging = false;

            toolsElement.on({
                mousedown: function () {
                    startDragging = true;
                    settings.onDragStart();
                }
            });

            attachedElement.on({
                mousemove: function (e) {

                    if (startDragging) {
                        var attachedElementOffset = attachedElement.offset();
                        var relX = e.pageX - attachedElementOffset.left;
                        var relY = e.pageY - attachedElementOffset.top;

                        var relXPercent = (relX / attachedElement.width()) * 100 + "%";
                        var relYPercent = (relY / attachedElement.height()) * 100 + "%";

                        if(settings.direction == 'vertical'){
                            frontElement.css({
                                "height": relYPercent
                            });
                            toolsElement.css({
                                "top": relYPercent
                            });
                        }else{
                            frontElement.css({
                                "width": relXPercent
                            });
                            toolsElement.css({
                                "left": relXPercent
                            });
                        }


                        if (toolsElement.css("left").replace(/[^-\d\.]/g, '') >= attachedElement.width()) {
                            startDragging = false;
                        }

                        //console.log(toolsElement.css("left") + " : " + attachedElement.width())

                        settings.isDragging();
                    }

                },
                mouseup: function () {
                    if (startDragging) {
                        settings.onDragStop();
                    }
                    startDragging = false;
                },
                click: function (e) {
                    var attachedElementOffset = attachedElement.offset();
                    var relX = e.pageX - attachedElementOffset.left;
                    var relY = e.pageY - attachedElementOffset.top;

                    if (settings.allowClick) {
                        var relXPercent = (relX / attachedElement.width()) * 100 + "%";
                        var relYPercent = (relY / attachedElement.height()) * 100 + "%";

                        if(settings.direction == 'vertical'){
                            frontElement.animate({
                                "height": relYPercent
                            }, settings.animationSpeed, settings.animationEasing);

                            toolsElement.animate({
                                "top": relYPercent
                            }, settings.animationSpeed, settings.animationEasing);
                        }else{
                            frontElement.animate({
                                "width": relXPercent
                            }, settings.animationSpeed, settings.animationEasing);

                            toolsElement.animate({
                                "left": relXPercent
                            }, settings.animationSpeed, settings.animationEasing);
                        }

                        settings.isDragging();
                    }

                }
            });

            if (settings.preventImageDrag) {

                frontElement.on("dragstart", function (e) {
                    e.preventDefault();
                });

                backElement.on("dragstart", function (e) {
                    e.preventDefault();
                });

            }



        });

        $(window).on("resize", function () {

            frontElement.find(settings.frontElementImage).css({
                "width": attachedElement.width()
            });

            backElement.find(settings.backElementImage).css({
                "width": attachedElement.width()
            });

            if(!settings.freeHeight){
                attachedElement.css({
                    "height": frontElement.height()
                });
            }

        });

        return this;

    };

}(jQuery));